<!--- Author : Posthemes.com -->
<div id="instagram_block_home">	<div class="container">
		<div class="pos_title">
			<h2>{l s='Instagram' mod='posinstagramblock'}</h2>			<div class="desc_title">				{l s='See Instagram photos and videos' mod='posinstagramblock'}			</div>
		</div>
		<div class="row pos_content">
			<div class="instagram_block owl-carousel">
				{foreach from=$instagrams item=instagram name=myLoop}
				{if $smarty.foreach.myLoop.index % 1 == 0 || $smarty.foreach.myLoop.first }
				<div class="instagram-block">
				{/if}
					<div class="item-instagram">
						<a class="fancybox" data-fancybox="gallery" href="{$instagram['image']}" style="display: block;"><img src="{$instagram['low_resolution']}" alt="" /></a>
					</div>
				{if $smarty.foreach.myLoop.iteration % 1 == 0 || $smarty.foreach.myLoop.last  }
				</div>
				{/if}
				{/foreach}
			</div>
		</div>
	</div></div>
<script>    
$(document).ready(function () {		
	$("#instagram_block_home .instagram_block").owlCarousel({		nav : true,		dots : false,		responsiveClass:true,				responsive:{			0:{				items:1,			},			480:{				items:2,			},			576:{				items:2,			},			768:{				items:3,			},			992:{				items:4,			},			1200:{				items:{$home_items},			}		}
	});
	$('#instagram_block_home [data-fancybox="gallery"]').fancybox({		// Options will go here	});
						
});
</script>


