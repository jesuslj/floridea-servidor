<?php
/* Smarty version 3.1.33, created on 2021-05-05 10:19:31
  from '/var/www/floridea/html/themes/theme_corano4/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6092b783dc1046_57232838',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a4e733771e1023f0a9c8a0d66b3ef17e5d814375' => 
    array (
      0 => '/var/www/floridea/html/themes/theme_corano4/templates/index.tpl',
      1 => 1620162002,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6092b783dc1046_57232838 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13531558316092b783dbe7f5_60425472', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_11194362596092b783dbeef2_32807635 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_6270333206092b783dbfbe6_34592374 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_14551661826092b783dbf769_21422225 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6270333206092b783dbfbe6_34592374', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_13531558316092b783dbe7f5_60425472 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_13531558316092b783dbe7f5_60425472',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_11194362596092b783dbeef2_32807635',
  ),
  'page_content' => 
  array (
    0 => 'Block_14551661826092b783dbf769_21422225',
  ),
  'hook_home' => 
  array (
    0 => 'Block_6270333206092b783dbfbe6_34592374',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11194362596092b783dbeef2_32807635', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14551661826092b783dbf769_21422225', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
